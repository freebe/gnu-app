package com.gnu_app;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

public class Meal extends Activity implements OnClickListener {

	Message message;
	ImageView btn1, btn2, btn3, btn4, btn5, btn6;

	private ImageView Swap[][] = new ImageView[6][2];

	private View view[][] = new View[6][2];

	private TextView tv1[][] = new TextView[4][6];
	private TextView tv2[][] = new TextView[6][6];
	private TextView tv3[][] = new TextView[2][6];
	private TextView tv4[][] = new TextView[2][5];
	private TextView tv5[][] = new TextView[2][6];
	private TextView tv6[][] = new TextView[4][5];

	private String[][] parsed1 = new String[4][6];
	private String[][] parsed2 = new String[6][6];
	private String[][] parsed3 = new String[2][6];
	private String[][] parsed4 = new String[2][5];
	private String[][] parsed5 = new String[2][6];
	private String[][] parsed6 = new String[4][6];

	private Handler mHandler;
	private String AA = null;
	int i, j;
	private boolean flag1 = false, flag2 = false, flag3 = false, flag4 = false,
			flag5 = false, flag6 = false;
	private int where = 0;
	private boolean tab = false;
	LinearLayout layout;
	MealClick Click1;
	MealClick Click2;
	MealClick Click3;
	MealClick Click4;
	MealClick Click5;
	MealClick Click6;
	RemoteViews remoteViews;
	Context context;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 제목표시줄 제거
		setContentView(R.layout.mealclick);
		mHandler = new connecthandler();

		// 상단 버튼 등록
		btn1 = (ImageView) findViewById(R.id.btn1);
		btn2 = (ImageView) findViewById(R.id.btn2);
		btn3 = (ImageView) findViewById(R.id.btn3);
		btn4 = (ImageView) findViewById(R.id.btn4);
		btn5 = (ImageView) findViewById(R.id.btn5);
		btn6 = (ImageView) findViewById(R.id.btn6);

		for (int i = 0; i < 6; i++) { // Swap btn
			for (int j = 0; j < 2; j++) {
				int resID = getResources()
						.getIdentifier(
								getPackageName() + ":id/swap" + (i + 1) + "_"
										+ (j + 1), null, null);
				Swap[i][j] = (ImageView) findViewById(resID);
				Swap[i][j].setOnClickListener(this);
			}
		}

		btn1.setOnClickListener(this);
		btn2.setOnClickListener(this);
		btn3.setOnClickListener(this);
		btn4.setOnClickListener(this);
		btn5.setOnClickListener(this);
		btn6.setOnClickListener(this);

		for (int i = 0; i < 4; i++) { // Res 1
			for (int j = 0; j < 6; j++) {
				int resID = getResources().getIdentifier(
						getPackageName() + ":id/view1_" + (i + 1) + "_"
								+ (j + 1), null, null);
				tv1[i][j] = (TextView) findViewById(resID);
			}
		}
		
		for (int i = 0; i < 6; i++) { // Res 2
			for (int j = 0; j < 6; j++) {
				int resID = getResources().getIdentifier(
						getPackageName() + ":id/view2_" + (i + 1) + "_"
								+ (j + 1), null, null);
				tv2[i][j] = (TextView) findViewById(resID);
			}
		}

		for (int i = 0; i < 2; i++) { // Res 3
			for (int j = 0; j < 6; j++) {
				int resID = getResources().getIdentifier(
						getPackageName() + ":id/view3_" + (i + 1) + "_"
								+ (j + 1), null, null);
				tv3[i][j] = (TextView) findViewById(resID);
			}
		}
		
		for (int i = 0; i < 2; i++) { // Res 4
			for (int j = 0; j < 5; j++) {
				int resID = getResources().getIdentifier(
						getPackageName() + ":id/view4_" + (i + 1) + "_"
								+ (j + 1), null, null);
				tv4[i][j] = (TextView) findViewById(resID);
			}
		}
		
		for (int i = 0; i < 2; i++) { // Res 5
			for (int j = 0; j < 6; j++) {
				int resID = getResources().getIdentifier(
						getPackageName() + ":id/view5_" + (i + 1) + "_"
								+ (j + 1), null, null);
				tv5[i][j] = (TextView) findViewById(resID);
			}
		}
		
		for (int i = 0; i < 4; i++) { // Res 6
			for (int j = 0; j < 5; j++) {
				int resID = getResources().getIdentifier(
						getPackageName() + ":id/view6_" + (i + 1) + "_"
								+ (j + 1), null, null);
				tv6[i][j] = (TextView) findViewById(resID);
			}
		}

		// 각 그룹 뷰 등록

		view[0][0] = (View) findViewById(R.id.view1_pre);
		view[0][1] = (View) findViewById(R.id.view1_next);

		view[1][0] = (View) findViewById(R.id.view2_pre);
		view[1][1] = (View) findViewById(R.id.view2_next);

		view[2][0] = (View) findViewById(R.id.view3_pre);
		view[2][1] = (View) findViewById(R.id.view3_next);

		view[3][0] = (View) findViewById(R.id.view4_pre);
		view[3][1] = (View) findViewById(R.id.view4_next);

		view[4][0] = (View) findViewById(R.id.view5_pre);
		view[4][1] = (View) findViewById(R.id.view5_next);

		view[5][0] = (View) findViewById(R.id.view6_pre);
		view[5][1] = (View) findViewById(R.id.view6_next);

		layout = (LinearLayout) findViewById(R.id.MainLayout);

		Init();
	}

	@Override
	public void onClick(View v) {
		//Swap[0][0].setImageResource(R.drawable.arrow);
		layout.setBackgroundColor(Color.rgb(204,204,204));
		switch (v.getId()) {

		case R.id.btn1:
			View1();
			break;

		case R.id.btn2:
			View2();
			break;

		case R.id.btn3:
			View3();
			break;

		case R.id.btn4:
			View4();
			break;

		case R.id.btn5:
			View5();
			break;

		case R.id.btn6:
			View6();
			break;

		case R.id.swap1_1:
		case R.id.swap1_2:
			ViewClick(1);
			break;

		case R.id.swap2_1:
		case R.id.swap2_2:
			ViewClick(2);
			break;

		case R.id.swap3_1:
		case R.id.swap3_2:
			ViewClick(3);
			break;

		case R.id.swap4_1:
		case R.id.swap4_2:
			ViewClick(4);
			break;

		case R.id.swap5_1:
		case R.id.swap5_2:
			ViewClick(5);
			break;

		case R.id.swap6_1:
		case R.id.swap6_2:
			ViewClick(6);
			break;

		}

	}

	private void ViewClick(int check) {
		switch (check) {
		case 1:
			if (tab == false) {
				view[0][0].setVisibility(View.GONE);
				view[0][1].setVisibility(View.VISIBLE);
				tab = true;
				Swap[0][1].setImageResource(R.drawable.arrow);
			} else {
				view[0][1].setVisibility(View.GONE);
				view[0][0].setVisibility(View.VISIBLE);
				tab = false;
			}

			break;
		case 2:

			if (tab == false) {
				view[1][0].setVisibility(View.GONE);
				view[1][1].setVisibility(View.VISIBLE);
				tab = true;
				Swap[1][1].setImageResource(R.drawable.arrow);
			} else {
				view[1][1].setVisibility(View.GONE);
				view[1][0].setVisibility(View.VISIBLE);
				tab = false;
			}

			break;

		case 3:

			if (tab == false) {
				view[2][0].setVisibility(View.GONE);
				view[2][1].setVisibility(View.VISIBLE);
				tab = true;
				Swap[2][1].setImageResource(R.drawable.arrow);
			} else {
				view[2][1].setVisibility(View.GONE);
				view[2][0].setVisibility(View.VISIBLE);
				tab = false;
			}

			break;

		case 4:

			if (tab == false) {
				view[3][0].setVisibility(View.GONE);
				view[3][1].setVisibility(View.VISIBLE);
				tab = true;
				Swap[3][1].setImageResource(R.drawable.arrow);
			} else {
				view[3][1].setVisibility(View.GONE);
				view[3][0].setVisibility(View.VISIBLE);
				tab = false;
			}

			break;

		case 5:

			if (tab == false) {
				view[4][0].setVisibility(View.GONE);
				view[4][1].setVisibility(View.VISIBLE);
				tab = true;
				Swap[4][1].setImageResource(R.drawable.arrow);
			} else {
				view[4][1].setVisibility(View.GONE);
				view[4][0].setVisibility(View.VISIBLE);
				tab = false;
			}

			break;

		case 6:

			if (tab == false) {
				view[5][0].setVisibility(View.GONE);
				view[5][1].setVisibility(View.VISIBLE);
				tab = true;
				Swap[5][1].setImageResource(R.drawable.arrow);
			} else {
				view[5][1].setVisibility(View.GONE);
				view[5][0].setVisibility(View.VISIBLE);
				tab = false;
			}

			break;
		}
	}

	private void ViewSet(int check) {
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 2; j++) {
				view[i][j].setVisibility(View.GONE);
			}
		}

		if (check == 1)
			view[0][0].setVisibility(View.VISIBLE);// Char Anchor (10,25)
		if (check == 2)
			view[1][0].setVisibility(View.VISIBLE);// Char Anchor (6,25)
		if (check == 3)
			view[2][0].setVisibility(View.VISIBLE);
		if (check == 4)
			view[3][0].setVisibility(View.VISIBLE);// Char Anchor (12,14)
		if (check == 5)
			view[4][0].setVisibility(View.VISIBLE);// Char Anchor (13,25)
		if (check == 6)
			view[5][0].setVisibility(View.VISIBLE);// Char Anchor (3,25)

	}

	private void Init() {
		ViewSet(0);
		btn1.setImageResource(R.drawable.btn_1_not);
		btn2.setImageResource(R.drawable.btn_2_not);
		btn3.setImageResource(R.drawable.btn_3_not);
		btn4.setImageResource(R.drawable.btn_4_not);
		btn5.setImageResource(R.drawable.btn_5_not);
		btn6.setImageResource(R.drawable.btn_6_not);
	}

	private void View1() {
		ViewSet(1);
		Swap[0][0].setImageResource(R.drawable.arrow);
		btn1.setImageResource(R.drawable.btn_1_over);
		btn2.setImageResource(R.drawable.btn_2);
		btn3.setImageResource(R.drawable.btn_3_not);
		btn4.setImageResource(R.drawable.btn_4_not);
		btn5.setImageResource(R.drawable.btn_5_not);
		btn6.setImageResource(R.drawable.btn_6_not);
		where = 1;
		parsing('A', flag1);
	}

	private void View2() {
		ViewSet(2);
		Swap[1][0].setImageResource(R.drawable.arrow);
		btn1.setImageResource(R.drawable.btn_1);
		btn2.setImageResource(R.drawable.btn_2_over);
		btn3.setImageResource(R.drawable.btn_3);
		btn4.setImageResource(R.drawable.btn_4_not);
		btn5.setImageResource(R.drawable.btn_5_not);
		btn6.setImageResource(R.drawable.btn_6_not);
		where = 2;
		parsing('B',flag2);
	}

	private void View3() {
		ViewSet(3);
		Swap[2][0].setImageResource(R.drawable.arrow);
		btn1.setImageResource(R.drawable.btn_1_not);
		btn2.setImageResource(R.drawable.btn_2_open);
		btn3.setImageResource(R.drawable.btn_3_over);
		btn4.setImageResource(R.drawable.btn_4);
		btn5.setImageResource(R.drawable.btn_5_not);
		btn6.setImageResource(R.drawable.btn_6_not);
		where = 3;
		parsing('C', flag3);
	}

	private void View4() {
		ViewSet(4);
		Swap[3][0].setImageResource(R.drawable.arrow);
		btn1.setImageResource(R.drawable.btn_1_not);
		btn2.setImageResource(R.drawable.btn_2_not);
		btn3.setImageResource(R.drawable.btn_3_open);
		btn4.setImageResource(R.drawable.btn_4_over);
		btn5.setImageResource(R.drawable.btn_5);
		btn6.setImageResource(R.drawable.btn_6_not);
		where = 4;
		parsing('D', flag4);
	}

	private void View5() {
		ViewSet(5);
		Swap[4][0].setImageResource(R.drawable.arrow);
		btn1.setImageResource(R.drawable.btn_1_not);
		btn2.setImageResource(R.drawable.btn_2_not);
		btn3.setImageResource(R.drawable.btn_3_not);
		btn4.setImageResource(R.drawable.btn_4_open);
		btn5.setImageResource(R.drawable.btn_5_over);
		btn6.setImageResource(R.drawable.btn_6);
		where = 5;
		parsing('E', flag5);
	}

	private void View6() {
		ViewSet(6);
		Swap[5][0].setImageResource(R.drawable.arrow);
		btn1.setImageResource(R.drawable.btn_1_not);
		btn2.setImageResource(R.drawable.btn_2_not);
		btn3.setImageResource(R.drawable.btn_3_not);
		btn4.setImageResource(R.drawable.btn_4_not);
		btn5.setImageResource(R.drawable.btn_5_open);
		btn6.setImageResource(R.drawable.btn_6_over);
		where = 6;
		parsing('F', flag6);
	}

	private void parsing(final char which, boolean flag) {
		if (flag == false) {
			new Thread() {
				public void run() {
					HttpURLConnection Connect = null;
					Source source = null;
					String url = "http://service.gnu.ac.kr/program/foodmenu/foodmenu.jsp?restaurant="
							+ which;
					try {
						URL Mealtable = new URL(url);
						Connect = (HttpURLConnection) Mealtable
								.openConnection();
						source = new Source(Connect);
						Connect.connect();
						source.fullSequentialParse();
						Element tr, td;
						Element table = source.getAllElements(
								HTMLElementName.TABLE).get(0);
						if (which == 'A') {
							for (i = 1; i < 5; i++) {
								tr = table.getAllElements(HTMLElementName.TR)
										.get(i);

								for (j = 0; j < 6; j++) {
									td = tr.getAllElements(HTMLElementName.TD)
											.get(j);
									AA = td.getTextExtractor().toString();
									parsed1[i - 1][j] = AA;
								}
							}
							message = mHandler.obtainMessage(1);
						}
						
						else if (which == 'B') {
							for (i = 1; i < 7; i++) {
								tr = table.getAllElements(HTMLElementName.TR)
										.get(i);

								for (j = 0; j < 6; j++) {
									td = tr.getAllElements(HTMLElementName.TD)
											.get(j);
									AA = td.getTextExtractor().toString();
									parsed2[i - 1][j] = AA;
								}
							}
							message = mHandler.obtainMessage(2);
						}
						

						else if (which == 'C') {
							for (i = 1; i < 3; i++) {
								tr = table.getAllElements(HTMLElementName.TR)
										.get(i);

								for (j = 0; j < 6; j++) {
									td = tr.getAllElements(HTMLElementName.TD)
											.get(j);
									AA = td.getTextExtractor().toString();
									parsed3[i - 1][j] = AA;
								}
							}
							message = mHandler.obtainMessage(3);
						}
						
						else if (which == 'D') {
							for (i = 1; i < 3; i++) {
								tr = table.getAllElements(HTMLElementName.TR)
										.get(i);

								for (j = 0; j < 5; j++) {
									td = tr.getAllElements(HTMLElementName.TD)
											.get(j);
									AA = td.getTextExtractor().toString();
									parsed4[i - 1][j] = AA;
								}
							}
							message = mHandler.obtainMessage(4);
						}
						
						else if (which == 'E') {
							for (i = 1; i < 3; i++) {
								tr = table.getAllElements(HTMLElementName.TR)
										.get(i);

								for (j = 0; j < 6; j++) {
									td = tr.getAllElements(HTMLElementName.TD)
											.get(j);
									AA = td.getTextExtractor().toString();
									parsed5[i - 1][j] = AA;
								}
							}
							message = mHandler.obtainMessage(5);
						}
						
						else if (which == 'F') {
							for (i = 1; i < 5; i++) {
								tr = table.getAllElements(HTMLElementName.TR)
										.get(i);

								for (j = 0; j < 5; j++) {
									td = tr.getAllElements(HTMLElementName.TD)
											.get(j);
									AA = td.getTextExtractor().toString();
									parsed6[i - 1][j] = AA;
								}
							}
							message = mHandler.obtainMessage(6);
						}
						else{}
						mHandler.sendMessage(message);
					} catch (IOException e) {
						mHandler.sendEmptyMessage(9);
						e.printStackTrace();
					}
				}
			}.start();
		}
		else{
			message = mHandler.obtainMessage(7);
		}
	}

	@SuppressLint("HandlerLeak")
	class connecthandler extends Handler { // 접속 핸들러 클래스
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1:
				for (int i = 0; i < 4; i++) {
					for (int j = 0; j < 6; j++) {
						tv1[i][j].setText(parsed1[i][j]);
						tv1[i][j].setVerticalScrollBarEnabled(true); //와 쩐다 ;;
						tv1[i][j].setMovementMethod(new ScrollingMovementMethod());
					}
				}
				flag1 = true;
				mHandler.sendEmptyMessage(8);
				break;

			case 2:
				for (int i = 0; i < 6; i++) {
					for (int j = 0; j < 6; j++) {
						tv2[i][j].setText(parsed2[i][j]);
						tv2[i][j].setVerticalScrollBarEnabled(true); //와 쩐다 ;;
						tv2[i][j].setMovementMethod(new ScrollingMovementMethod());
					}
				}
				flag2 = true;
				mHandler.sendEmptyMessage(8);
				break;

			case 3:
				for (int i = 0; i < 2; i++) {
					for (int j = 0; j < 6; j++) {
						tv3[i][j].setText(parsed3[i][j]);
						tv3[i][j].setVerticalScrollBarEnabled(true); //와 쩐다 ;;
						tv3[i][j].setMovementMethod(new ScrollingMovementMethod());
					}
				}
				flag3 = true;
				mHandler.sendEmptyMessage(8);
				break;

			case 4:
				for (int i = 0; i < 2; i++) {
					for (int j = 0; j < 5; j++) {
						tv4[i][j].setText(parsed4[i][j]);
						tv4[i][j].setVerticalScrollBarEnabled(true); //와 쩐다 ;;
						tv4[i][j].setMovementMethod(new ScrollingMovementMethod());
					}
				}
				flag4 = true;
				mHandler.sendEmptyMessage(8);
				break;

			case 5:
				for (int i = 0; i < 2; i++) {
					for (int j = 0; j < 6; j++) {
						tv5[i][j].setText(parsed5[i][j]);
						tv5[i][j].setVerticalScrollBarEnabled(true); //와 쩐다 ;;
						tv5[i][j].setMovementMethod(new ScrollingMovementMethod());
					}
				}
				flag5 = true;
				mHandler.sendEmptyMessage(8);
				break;

			case 6:
				for (int i = 0; i < 4; i++) {
					for (int j = 0; j < 5; j++) {
						tv6[i][j].setText(parsed6[i][j]);
						tv6[i][j].setVerticalScrollBarEnabled(true); //와 쩐다 ;;
						tv6[i][j].setMovementMethod(new ScrollingMovementMethod());
					}
				}
				flag6 = true;
				mHandler.sendEmptyMessage(8);
				break;
				
			case 7:
				Toast.makeText(getApplicationContext(), "받아와져 있지롱",
						Toast.LENGTH_SHORT).show();
				break;
				
			case 8:
				Toast.makeText(getApplicationContext(), "데이터를 받아왔습니다.",
						Toast.LENGTH_SHORT).show();
				break;

			default:
				Toast.makeText(getApplicationContext(), "인터넷 접속이 원활하지 않습니다.",
						Toast.LENGTH_SHORT).show();
				break;
			}
		}
	}

}