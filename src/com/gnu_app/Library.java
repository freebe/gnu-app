package com.gnu_app;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputType;
import android.view.KeyEvent;
//import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.gnu_app.R.layout;

public class Library extends Activity implements OnItemSelectedListener {

	private ArrayList<String> arraylist;
	private ArrayList<String> arrayresult;
	private ArrayAdapter<String> adapter;
	//private Spinner Spinner;
	private EditText Editbox;
	private Handler mhandler;
	private String ID;
	private String Keyword;
	private ListView Listview;
	static HttpURLConnection Connect = null;
	private String extra;
	private String extraArray[] = new String[300];
	private String extraID[] = new String[300];
	private int fill=0;
	//private LayoutInflater mInflater;
	private String Temp=null;
	private ProgressDialog dialog;
	private boolean flag=false;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 제목표시줄 제거
		setContentView(R.layout.library);

		//Spinner = (Spinner) findViewById(R.id.spinner1);
		Editbox = (EditText) findViewById(R.id.input);
		Listview = (ListView) findViewById(R.id.listView);
		Editbox.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
		Editbox.setInputType(InputType.TYPE_CLASS_TEXT);
		arraylist = new ArrayList<String>();
		arraylist.add("제목");
		arraylist.add("저자");
		ID = "TITL";
		
		arrayresult = new ArrayList<String>();
		
		mhandler = new connecthandler();

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, arraylist);
		// 스피너 속성
		Spinner sp = (Spinner) this.findViewById(R.id.spinner1);
		sp.setAdapter(adapter);
		sp.setOnItemSelectedListener(this);

		Editbox.setOnEditorActionListener(new OnEditorActionListener() {
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				switch (actionId) {
				case EditorInfo.IME_ACTION_SEARCH:
					Keyword = Editbox.getText().toString();
					Load();
					break;
				default:
					return false;
				}
				return true;
			}
		});
	}

	@SuppressLint("HandlerLeak")
	public class connecthandler extends Handler { // 접속 핸들러 클래스
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1:
				//Toast.makeText(getApplicationContext(),Editbox.getText().toString(), Toast.LENGTH_SHORT).show();
				flag=true;
				Toast.makeText(getApplicationContext(),"인터넷 연결이 원활하지 않습니다.", Toast.LENGTH_SHORT).show();
				break;
			case 2:
				Load();
				break;
			case 3:
				flag=true;
				View();
				break;
				
			case 4:
				flag=true;
				Toast.makeText(getApplicationContext(),"검색결과가 없습니다.",Toast.LENGTH_SHORT).show();
				break;
				
			default:
				Toast.makeText(getApplicationContext(),"디폴트",Toast.LENGTH_SHORT).show();
				break;
			}
		}
	}

	public void Load() {
		fill=0;
		flag=false;
		dialog = new ProgressDialog(this);
		dialog.setTitle("Wait...");
		dialog.setMessage("Please wait while loading...");
		dialog.setIndeterminate(true);
		dialog.setCancelable(true);
		dialog.show();
		dialog.setCancelable(false);
		new Thread(new Runnable() {
			public void run() {
				// TODO Auto-generated method stub
				try {
					while(!flag) Thread.sleep(1000);						
				} catch (Throwable ex) {
					//ex.printStackTrace();
				}
				if(flag==true) dialog.dismiss();
			}
		}).start();
        
		new Thread() {
			public void run() {
				Source source = null;
				try {
					//ID = URLEncoder.encode(ID);
					//Keyword = URLEncoder.encode(Keyword);
					String query = "m_var=421&srv_id=31&brch=ALL&loc=ALL&cl_id=ALL&qy_frm=ADVANCED&qy_typ=START_WITH"
								+ "&rid_all=1^2^3^4^5^31&rpp=200&mc=300&dp_op=LIST&qy_opt=AND&hdnDexMode=PAGE"
								+ "&qy_idx=" + ID + "&qy_kwd=" + Keyword + "&pg=1";
					String u = "http://library.gnu.ac.kr/DLiWeb20/components/searchir/result.aspx?";
					URL url = new URL(u);
					URLConnection connection = url.openConnection();
					HttpURLConnection hurlc = (HttpURLConnection)connection;
					hurlc.setRequestMethod("GET");
					hurlc.setDoOutput(true);
					hurlc.setDoInput(true);
					hurlc.setUseCaches(false);
					hurlc.setDefaultUseCaches(false);
					
					PrintWriter out = new PrintWriter(hurlc.getOutputStream());
					out.println(query);
					out.close();
					
					source = new Source(hurlc);
					
					source.fullSequentialParse();
					
					Element div = source.getElementById("tblSearchResult");
					
					if(div!=null){
								
					List<Element> AAA = div.getAllElements(HTMLElementName.TR);
							
					Object[] BBB = AAA.toArray();
					int start = 0;
					int end = 0;
					for(int temp=0,i=0; i < BBB.length/4; temp=temp+4, i++){
						String extra = BBB[temp].toString();
						start = extra.indexOf("trCID_");
						end = extra.indexOf("'>");
						extraID[i] = extra.substring(start+6,end);
					}
					
					
						extra = div.getTextExtractor().toString();
						//Pattern preview = Pattern.compile("미리보기");//ADD source pattern Matcher
						String AA = extra.replaceAll("미리보기 ", "\n");
						AA = AA.replaceAll("/ ", "\nMyToken");
						int i=2;
						int startwhere=0;
						int endwhere=0;
						
						
						while(AA.indexOf(" "+i+" ")!=-1 && i!=300){
							//endwhere = AA.indexOf(" "+i+" "); //end가 start보다 커야되고 다음 인덱스 찾기로 바꿔야 됨.
							for(endwhere = AA.indexOf(" "+i+" "); endwhere<startwhere; endwhere = AA.indexOf(" "+i+" ", endwhere+1)){}
							Temp = AA.substring(startwhere, endwhere);
							Temp = Temp.replaceFirst(i-1+" ", "제목 : ");
							Temp = Temp.replaceFirst("MyToken","저자 : ");//여기 temp = 을 안붙이니까 에러남 ;;
							Temp = Temp.replaceFirst("MyToken","출판사 : ");
							Temp = Temp.replaceFirst("MyToken","년도 : ");
							Temp = Temp.replaceFirst("MyToken소장위치"," 소장위치 : ");
							
							extraArray[fill] = Temp;
							i++;
							fill++;
							startwhere = endwhere;
						}
						Temp = AA.substring(startwhere, AA.length());
						Temp = Temp.replaceFirst(i-1+" ", "제목 : ");
						Temp = Temp.replaceFirst("MyToken","저자 : ");//여기 temp = 을 안붙이니까 에러남 ;;
						Temp = Temp.replaceFirst("MyToken","출판사 : ");
						Temp = Temp.replaceFirst("MyToken","년도 : ");
						Temp = Temp.replaceFirst("MyToken소장위치"," 소장위치 : ");
						extraArray[fill] = Temp;
						
						mhandler.sendEmptyMessage(3);
					}
					else{
						//dialog.dismiss();
						mhandler.sendEmptyMessage(4);
					}
					
					/*BufferedReader reader = new BufferedReader(new InputStreamReader(hurlc.getInputStream(), "UTF-8"));
					String line = null;

					StringBuilder sourcee = new StringBuilder();
					int count=0;
					
					while(true){
						line = reader.readLine();
						
						if(line == null){
							break;
						}
				 
						Pattern sourceP = Pattern.compile("<table id=\"tblSearchResult\" cellSpacing=\"0\" cellPadding=\"0\" width=\"690\" border=\"0\" >");//ADD source pattern Matcher
						Matcher matcher = sourceP.matcher(line);
						if(matcher.find()) count=1;

						if(count==1) sourcee.append(line + "\n");
						
						Pattern sourcePP = Pattern.compile("</table>");//ADD source pattern Matcher
						Matcher matcherr = sourcePP.matcher(line);
						if(matcherr.find() && count==1) count=2;

												
					}
					
					reader.close();*/
					/*
					 * URL Lib = new URL(url);
					 * Connect.setInstanceFollowRedirects( false ) ; Connect =
					 * (HttpURLConnection) Lib.openConnection();
					 * Connect.setRequestMethod("GET");
					 * Connect.setRequestProperty("Connection","Keep-Alive");
					 * Connect.setUseCaches(false); Connect.setDoInput(true);
					 * Connect.setRequestProperty("Cookie", cookies);
					 * Connect.connect(); source = new Source(Connect);
					 */
					
					/*String url = "http://library.gnu.ac.kr/DLiWeb20/components/searchir/result.aspx?";
					
					String data = URLEncoder.encode("m_var=421&srv_id=31&brch=ALL&loc=ALL&cl_id=ALL&qy_frm=ADVANCED&qy_typ=START_WITH"
							+ "&rid_all=1^2^3^4^5^31&rpp=200&mc=300&dp_op=LIST&qy_opt=AND&hdnDexMode=PAGE"
							+ "&qy_idx=" + ID + "&qy_kwd=" + Keyword + "&pg=1", "UTF-8");

					URL conn = new URL(url);
					Connect = (HttpURLConnection)conn.openConnection();
					
					Connect.setDoOutput(true);
					
					OutputStreamWriter wr = new OutputStreamWriter(Connect.getOutputStream());
				      wr.write(data);
				      wr.flush();
					*/
					
					/*+ "m_var=421&srv_id=31&brch=ALL&loc=ALL&cl_id=ALL&qy_frm=ADVANCED&qy_typ=START_WITH"
					+ "&rid_all=1^2^3^4^5^31&rpp=200&mc=300&dp_op=LIST&qy_opt=AND&hdnDexMode=PAGE"
					+ "&qy_idx=" + ID + "&qy_kwd=" + Keyword + "&pg=1";*/
					

					//HttpGet get = new HttpGet(url);
					//HttpResponse responseGET = new DefaultHttpClient().execute(get);
					//HttpEntity resEntity = responseGET.getEntity();
					
					//DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					//DocumentBuilder builder = factory.newDocumentBuilder();
					//InputStreamReader is = new InputStreamReader(url.openStream());
					//Document document = builder.parse(is);
					//Element order = (Element)document.getDocumentElement();
					//Log.i("Sdfwe","WAef");
					
					  /*SAXParserFactory parserFactory = SAXParserFactory.newInstance();
					  SAXParser parser = parserFactory.newSAXParser();
					  XMLReader reader = parser.getXMLReader();
					  reader.parse(new InputSource(conn.openStream()));*/
					/*BufferedReader reader = new BufferedReader(new InputStreamReader(Connect.getInputStream(), "UTF-8"));
					String line = null;

					StringBuilder sourcee = new StringBuilder();
					int count=0;
					
					while(true){
						line = reader.readLine();
						
						if(line == null){
							break;
						}
				 
						Pattern sourceP = Pattern.compile("<table id=\"tblSearchResult\" cellSpacing=\"0\" cellPadding=\"0\" width=\"690\" border=\"0\" >");//ADD source pattern Matcher
						Matcher matcher = sourceP.matcher(line);
						if(matcher.find()) count=1;

						if(count==1) sourcee.append(line + "\n");
						
						Pattern sourcePP = Pattern.compile("</table>");//ADD source pattern Matcher
						Matcher matcherr = sourcePP.matcher(line);
						if(matcherr.find() && count==1) count=2;

												
					}
					
					reader.close();*/
					 

					//HttpURLConnection con =(HttpURLConnection)conn.openConnection();
					//con.addRequestProperty("Content-Type", "text/xml; charset=utf-8");
					 //InputStreamReader reader = new InputStreamReader(new BOMInputStream(conn.getInputStream(), false, ByteOrderMark.UTF_8));

					// new ParserDelegator().parse(reader, new
					// CallbackHandler(), true);

					// Element table
					// =source.getAllElements(HTMLElementName.TR).get(32);

					// Element e = (Element)
					// source.getElementById("tblSearchResult");
					// System.out.println("hogeId.extractText=" + e.toString());

					/*
					 * Element tr, td; Element table =
					 * source.getAllElements(HTMLElementName.TABLE).get(32);
					 * Element table2 =
					 * source.getAllElements(HTMLElementName.TABLE).get(32);
					 * 
					 * source.g //cookies =
					 * Connect.getHeaderField("Set-Cookie"); int tr_count =
					 * table.getAllElements(HTMLElementName.TR).size();
					 * 
					 * for (int i = 1; i < tr_count; i++) { tr =
					 * table.getAllElements(HTMLElementName.TR).get(i);
					 * 
					 * AA = tr.toString(); Pattern sourceP =
					 * Pattern.compile("'> ");//ADD source pattern Matcher
					 * matcher = sourceP.matcher(AA);
					 * AA=AA.substring(8,matcher.end()); }
					 */
				} catch (IOException e) {
					mhandler.sendEmptyMessage(1);
					e.printStackTrace();
				}
			}
		}.start();
	}

	public void View() {
		new Thread() {
			public void run() {
				arrayresult = new ArrayList<String>();
				for(int i = 0 ; i<fill+1; i++){
					arrayresult.add(extraArray[i]);
				}
				adapter = new ArrayAdapter<String>(getApplicationContext(), layout.listview, arrayresult);
		        // 리스트뷰 객체 생성 & 어댑터 설정
				Listview = (ListView)findViewById(R.id.listView);
				//runOnUIThread();//리스트뷰 출력부분
				runOnUIThread();
				//TextView view1 = (TextView)findViewById(R.id.scrolltv1);
	        	//view1.setText(adapter.toString());
				Listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		        // 리스트뷰에 아이템 클릭 리스너 부착
				Listview.setOnItemClickListener(new OnItemClickListener(){
		            @Override
		            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		                // 아이템을 터치하면 토스트를 띄운다.
		                String str = (String)adapter.getItem(position);
		                //Toast.makeText(getBaseContext(), str + extraID[adapter.getPosition(str)], Toast.LENGTH_SHORT).show();
		                Intent intent = new Intent(getBaseContext(), DetailLib.class);
		                intent.putExtra("ID", extraID[adapter.getPosition(str)]);
		                startActivity(intent);
		                }
		        });
			}

			private void runOnUIThread() {
				runOnUiThread(new Runnable() {
			        public void run() {
			        	Listview.setAdapter(adapter);
			        }
			    });				
			}
		}.start();
	}

	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// Toast.makeText(this, arraylist.get(arg2),
		// Toast.LENGTH_LONG).show();//해당목차눌렸을때
		String array = arraylist.get(arg2).toString();

		if (array == "저자")
			ID = "AUTH";
		if (array == "제목")
			ID = "TITL";

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}
	
	public void onBackPressed()  {
		super.onBackPressed();
	    
	        Thread.currentThread();
	        Thread.interrupted();
	    
	}
	
}
