package com.gnu_app;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;
 
//DB를 총괄관리
public class TableDataManager {
 
    // DB관련 상수 선언
    private static final String dbName = "TimeTable.db";
    private static final String tableName = "Schedule";
    public static final int dbVersion = 1;
 
    // DB관련 객체 선언
    private OpenHelper opener; // DB opener
    private SQLiteDatabase db; // DB controller
 
    // 부가적인 객체들
    private Context context;
 
    // 생성자
    public TableDataManager(Context context) {
        this.context = context;
        this.opener = new OpenHelper(context, dbName, null, dbVersion);
        db = opener.getWritableDatabase();
    }
 
    // Opener of DB and Table
    private class OpenHelper extends SQLiteOpenHelper {
 
        public OpenHelper(Context context, String name, CursorFactory factory,
                int version) {
            super(context, name, null, version);
            // TODO Auto-generated constructor stub
        }
 
        // 생성된 DB가 없을 경우에 한번만 호출됨
        @Override
        public void onCreate(SQLiteDatabase arg0) {
            String createSql = "create table " + tableName + " ("
                    + "id integer primary key autoincrement, " + "Subject text)";
            arg0.execSQL(createSql);
            Toast.makeText(context, "DB is opened", 0).show();
        }
 
        @Override
        public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
            // TODO Auto-generated method stub
        }
    }
 
    // 데이터 추가
    public void insertData(String[][] info) {
    	
        String sql;
        
        if(db!=null){
        	sql = "DROP TABLE IF EXISTS '" + tableName + "'";
        	db.execSQL(sql);
        	sql = "create table " + tableName + " ("
            + "id integer primary key autoincrement, " + "Subject text)";
        	db.execSQL(sql);
        }
        
        
        for(int i=0; i<13; i++){
        	for(int j=0; j<7; j++){
        	sql = "insert into " + tableName + " values(NULL, '"
                    + info[i][j] + "');";
        	db.execSQL(sql);
        	
        	}
        }
        
    	Log.i("sdfsd",info[0][0].toString());

    }
}
