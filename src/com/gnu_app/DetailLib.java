package com.gnu_app;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DetailLib extends Activity {

	private String ID;
	private String detail2;
	private String pic;
	private String picsource;
	private Handler mHandler;
	private Bitmap bitmap;
	private ImageView detailImage;
	private String Array[];
	/*private ArrayList<String> arrayresult1;
	private ArrayList<String> arrayresult2;
	private ArrayList<String> arrayresult3;
	private ArrayAdapter<String> adapter1;
	private ArrayAdapter<String> adapter2;
	private ArrayAdapter<String> adapter3;
	private ListView Listview1;
	private ListView Listview2;
	private ListView Listview3;
	private LayoutInflater mInflater;
	private View convertView;*/
	private MultiAdapter MyAdapter;
	private ListView MyList;
	private Element div;
	private ProgressDialog dialog;
	private boolean flag;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 제목표시줄 제거
		setContentView(R.layout.detail);
		
		mHandler = new connecthandler();
		
		
		detailImage = (ImageView) findViewById(R.id.detailImage);

		Intent intent = getIntent();
		ID = intent.getStringExtra("ID"); // 넘긴 ID 받아오기

		// http://library.gnu.ac.kr/DLiWeb20/components/searchir/detail/detail.aspx?cid=1109056
		flag=false;
		dialog = new ProgressDialog(this);
		dialog.setTitle("Wait...");
		dialog.setMessage("Please wait while loading...");
		dialog.setIndeterminate(true);
		dialog.setCancelable(true);
		dialog.show();
		dialog.setCancelable(false);
		
		new Thread(new Runnable() {
			public void run() {
				// TODO Auto-generated method stub
				try {
					while(!flag) Thread.sleep(1000);						
				} catch (Throwable ex) {
					//ex.printStackTrace();
				}
				if(flag==true) dialog.dismiss();
			}
		}).start();
		
		new Thread() {
			public void run() {
				Source source = null;
				try {
					// ID = URLEncoder.encode(ID);
					// Keyword = URLEncoder.encode(Keyword);
					String query = "cid=" + ID;
					String u = "http://library.gnu.ac.kr/DLiWeb20/components/searchir/detail/detail.aspx?";
					URL url = new URL(u);
					URLConnection connection = url.openConnection();
					HttpURLConnection hurlc = (HttpURLConnection) connection;
					hurlc.setRequestMethod("GET");
					hurlc.setDoOutput(true);
					hurlc.setDoInput(true);
					hurlc.setUseCaches(false);
					hurlc.setDefaultUseCaches(false);

					PrintWriter out = new PrintWriter(hurlc.getOutputStream());
					out.println(query);
					out.close();

					source = new Source(hurlc);

					source.fullSequentialParse();
					int size = 0;
					List<Element> els = source
							.getAllElements("td class='td_white_left'");
					
					Pattern sourceP = Pattern.compile("<a href");
					
					if (els.toString() != "[]") {
						size = els.size();
						Array = new String[size];
						for (int i = 0; i < size; i++) {
							Array[i] = els.get(i).toString();
							Matcher matcher = sourceP.matcher(Array[i]);
							if(matcher.find()){ // 예약 중 이거나 그런거 제거
								Array[i] = Array[i].substring(0,matcher.start());
							}
							Array[i] = Array[i].replaceAll(
									"<td class='td_white_left' >", "");

							Array[i] = Array[i].replaceAll(" </td>", "");
							Array[i] = Array[i].replaceAll("</td>", "");
						}
					} else {
						size=3;
						Array = new String[size];
						String temp="";
						int count=0;
						List els2 = source
								.getAllElements("tr align=\"center\"").get(0)
								.getAllElements("TD");

						Iterator trIter = els2.iterator();
						while (trIter.hasNext()) {
							Element data = (Element) trIter.next();
							temp = data.toString();
							temp = temp.replaceAll("<td>", "");
							temp = temp.replaceAll("</td>", "");
							if(count==1) Array[0] = temp;
							if(count==2) Array[1] = temp;
							if(count==3) Array[2] = temp;
							count++;
							
						}

						/*
						 * for(int j=4;j<7;j++){ els. td =
						 * els.getAllElements(HTMLElementName.TD).get(j);
						 * //해당<tr>안의 j번째 <td>의 내용을 td Element로 저장
						 * all[i-2][j]=td.getTextExtractor().toString(); //td값을
						 * String으로 all배열안에 저장함 }
						 */
						Log.i("sdfsdf", "sdfsdf");
					}

					// http://blog.naver.com/PostView.nhn?blogId=baram918&logNo=120133333021&categoryNo=6&viewDate=&currentPage=1&listtype=0
					// 야이 멘탈 날아감 안됨 씨ㅂㅂ

					ArrayList<ListItem> arItem;
					arItem = new ArrayList<ListItem>();
					for (int i = 0; i < size;) {
						arItem.add(new ListItem(Array[i], Array[i + 1],
								Array[i + 2]));
						i += 3;
					}

					MyAdapter = new MultiAdapter(getBaseContext(), arItem);
					MyList = (ListView) findViewById(R.id.wherelist);
					runOnUIThread();

					/*
					 * arrayresult1 = new ArrayList<String>(); arrayresult2 =
					 * new ArrayList<String>(); arrayresult3 = new
					 * ArrayList<String>();
					 * 
					 * for(int i = 0 ; i<size;){ arrayresult1.add(Array[i]);
					 * arrayresult2.add(Array[i+1]);
					 * arrayresult3.add(Array[i+2]); i+=3;
					 * 
					 * } adapter1 = new
					 * ArrayAdapter<String>(getApplicationContext(), layout.re,
					 * arrayresult1); adapter2 = new
					 * ArrayAdapter<String>(getApplicationContext(), layout.re,
					 * arrayresult2); adapter3 = new
					 * ArrayAdapter<String>(getApplicationContext(), layout.re,
					 * arrayresult3);
					 */

					div = source.getElementById("CoreFrame");
					if (div != null) {
						String extra = div.toString();
						int start = extra.indexOf("src=");
						int end = extra.indexOf("\"  ");
						detail2 = extra.substring(start + 5, end);

						picture(detail2);
					}
					mHandler.sendEmptyMessage(1);
					

				} catch (IOException e) {
					// mhandler.sendEmptyMessage(1);
					e.printStackTrace();
				}
			}

			private void runOnUIThread() {
				runOnUiThread(new Runnable() {
					public void run() {
						MyList.setAdapter(MyAdapter);
					}
				});
			}
		}.start();
	}

	class ListItem {
		String place;
		String number;
		String state;

		ListItem(String aplace, String anumber, String astate) {
			place = aplace;
			number = anumber;
			state = astate;
		}
	}

	class MultiAdapter extends BaseAdapter {

		LayoutInflater mInflater;
		ArrayList<ListItem> arSrc;

		public MultiAdapter(Context context, ArrayList<ListItem> arItem) {
			mInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			arSrc = arItem;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return arSrc.size();
		}

		@Override
		public ListItem getItem(int position) {
			// TODO Auto-generated method stub
			return arSrc.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			convertView = mInflater.inflate(R.layout.re, parent, false);
			TextView place = (TextView) convertView.findViewById(R.id.place);
			place.setText(arSrc.get(position).place);
			TextView number = (TextView) convertView.findViewById(R.id.number);
			number.setText(arSrc.get(position).state);
			TextView state = (TextView) convertView.findViewById(R.id.state);
			state.setText(arSrc.get(position).number);
			if(arSrc.get(position).number.toString().equals("대출가능")) state.setTextColor(Color.GREEN);
			else state.setTextColor(Color.RED);
			return convertView;
		}
	}

	public class connecthandler extends Handler { // 접속 핸들러 클래스
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1: // 이미지 받아오기
				flag=true;
				break;
			case 2:
				break;
			case 3:
				break;

			case 4:
				Toast.makeText(getApplicationContext(), "검색결과가 없습니다.",
						Toast.LENGTH_SHORT).show();
				break;

			default:
				Toast.makeText(getApplicationContext(), "디폴트",
						Toast.LENGTH_SHORT).show();
				break;
			}
		}
	}

	public void picture(final String detail2) {
		new Thread() {
			public void run() {
				Source source = null;
				try {
					String u = detail2;
					URL url = new URL(u);
					URLConnection connection = url.openConnection();
					connection.connect();

					source = new Source(connection);
					source.fullSequentialParse();

					pic = source.getAllElements(HTMLElementName.IMG).toString();
					int start = pic.indexOf("src='");
					int end = pic.indexOf("' ");
					picsource = pic.substring(start + 5, end);

					HttpGet httpRequest = new HttpGet(URI.create(picsource));
					HttpClient httpclient = new DefaultHttpClient();
					HttpResponse response = (HttpResponse) httpclient
							.execute(httpRequest);
					HttpEntity entity = response.getEntity();
					BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(
							entity);
					bitmap = BitmapFactory.decodeStream(bufHttpEntity
							.getContent());
					httpRequest.abort();

					runOnUIThread();
					

				} catch (IOException e) {
					// mhandler.sendEmptyMessage(1);
					e.printStackTrace();
				}
			}

			private void runOnUIThread() {
				runOnUiThread(new Runnable() {
					public void run() {
						detailImage.setImageResource(0);
						detailImage.setImageBitmap(bitmap);
					}
				});
			}
		}.start();
	}
}
