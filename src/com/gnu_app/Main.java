package com.gnu_app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;
import com.gnu_app.R;

import org.xmlpull.v1.XmlPullParserException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Main extends Activity {

	private Button LoginBtn;
	private Button Meal;
	//private Button Table;
	private Button Library;
	private EditText IDbox;
	private EditText PWbox;
	private String Timetable = "?";
	String IDText;
	String PWText;
	URL url;
	private String cookies = "";
	static HttpURLConnection Connect = null;
	Handler mHandler;
	boolean flag = false;
	boolean ConnectCheck = false;
	String[][] all = new String[13][7]; // String 형태의 배열
	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 제목표시줄 제거
		setContentView(R.layout.main);

		mHandler = new connecthandler();
		IDbox = (EditText) findViewById(R.id.ID);
		PWbox = (EditText) findViewById(R.id.PW);
		LoginBtn = (Button) findViewById(R.id.LoginBtn);
		Meal = (Button) findViewById(R.id.Meal);
		//Table = (Button) findViewById(R.id.TimeTable);테이블 보류
		Library = (Button) findViewById(R.id.Library);

		LoginBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					onClicked();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		Meal.setOnClickListener(new View.OnClickListener() { // 식단표
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Main.this, Meal.class);
				startActivity(intent);
			}
		});
		/*Table.setOnClickListener(new View.OnClickListener() { // 테이블 보류
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Main.this, Table.class);
				startActivity(intent);
			}
		});*/
		Library.setOnClickListener(new View.OnClickListener() { // 도서관
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Main.this, Library.class);
				startActivity(intent);
			}
		});
	}

	@SuppressLint("HandlerLeak")
	public class connecthandler extends Handler { // 접속 핸들러 클래스
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1:
				GetMenu(IDText, PWText, url);// 첫 접속
				break;

			case 2: // 플래그 교체
				Log.i("3", "3");
				Toast.makeText(getApplicationContext(), "ID나 PW가 맞지않습니다.",
						Toast.LENGTH_SHORT).show();
				flag = true;
				break;

			case 3:
				Toast.makeText(getApplicationContext(), "확인 후 다시 시도해 주세요",
						Toast.LENGTH_SHORT).show();
				break;

			case 4:
				GetTimeTable(msg.obj.toString());
				break;

			case 5:
				GetTimeTable2(msg.obj.toString());
				break;

			case 6:
				mRunnable.run();
				Toast.makeText(getApplicationContext(),
						"가져오기 성공" + '\n' + "위젯을 만들어 주세요", Toast.LENGTH_SHORT)
						.show();
				flag = true;
				ConnectCheck = true;
				break;

			case 7:
				Toast.makeText(getApplicationContext(), "시간표 정보가 없습니다.",
						Toast.LENGTH_SHORT).show();
				ConnectCheck = true;
				break;

			default:
				Log.i("4", "4");
				break;
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onClicked() throws XmlPullParserException, IOException {

		flag = false;
		ConnectCheck = false;

		IDText = IDbox.getText().toString();
		PWText = PWbox.getText().toString();
		IDText = IDText.trim(); // 공백제거
		PWText = PWText.trim();
		final ProgressDialog dialog;
		url = new URL("http://nis.gnu.ac.kr/sugang/");
		if (IDText.getBytes().length <= 0 || PWText.getBytes().length <= 0)
			Toast.makeText(this, "ID나 PW를 입력 해주세요", Toast.LENGTH_SHORT).show();
		else {
			// Toast.makeText(this, "Log-In 처리 중입니다.",
			// Toast.LENGTH_SHORT).show();
			dialog = new ProgressDialog(this);
			dialog.setTitle("Wait...");
			dialog.setMessage("Please wait while loading...");
			dialog.setIndeterminate(true);
			dialog.setCancelable(true);
			dialog.show();
			new Thread(new Runnable() {
				public void run() {
					// TODO Auto-generated method stub
					try {
						while (!flag)
							Thread.sleep(1000);
					} catch (Throwable ex) {
						ex.printStackTrace();
					}
					if (flag == true)
						dialog.dismiss();
					if (ConnectCheck == true) {
					} else {
						Message msg = mHandler.obtainMessage();
						mHandler.sendMessage(msg);
						mHandler.sendEmptyMessage(3);
					}
				}
			}).start();
			Message msg = mHandler.obtainMessage();
			mHandler.sendMessage(msg);
			mHandler.sendEmptyMessage(1);
		}
	}

	public void GetMenu(final String IDText, final String PWText, final URL url) {
		new Thread() {
			public void run() {
				Source source = null;
				try {
					String LoginURL = url.toString()
							+ "login?attribute=login&id=" + IDText
							+ "&password=" + PWText + "&student_gb=REGISTERED";
					URL LoginAdd = new URL(LoginURL);
					Connect = (HttpURLConnection) LoginAdd.openConnection();
					Connect.setDoOutput(true);
					Connect.setInstanceFollowRedirects(false);
					Connect.connect();
					source = new Source(Connect);
					List<Element> els = source.getAllElements();
					Pattern sourceP = Pattern.compile("src=\"(.*?)&f");// ADD
																		// source
																		// pattern
					Matcher matcher = sourceP.matcher(els.get(9).toString());
					String MenuSource, temp = "";
					while (matcher.find()) {
						MenuSource = els
								.get(9)
								.toString()
								.substring(matcher.start() + 24,
										matcher.end() - 2);
						Log.i("text",
								els.get(9)
										.toString()
										.substring(matcher.start(),
												matcher.end()));
						temp = MenuSource;
					}

					cookies = Connect.getHeaderField("Set-Cookie");
					Timetable = temp;
					Message message = mHandler.obtainMessage(4, Timetable);
					mHandler.sendMessage(message);
				} catch (Exception e) {
					Log.e("Merr", "Merr", e);
					Message message = mHandler.obtainMessage(2);
					mHandler.sendMessage(message);
				}
			}
		}.start();
	}

	public void GetTimeTable(final String Madd) {
		new Thread() {
			public void run() {
				Source source = null;
				try {

					String temp = URLEncoder.encode(Madd, "UTF-8");
					String TTURL = url.toString() + "filter?url=" + temp;
					URL Timetable = new URL(TTURL);
					Connect.setInstanceFollowRedirects(false);
					Connect = (HttpURLConnection) Timetable.openConnection();
					Connect.setRequestMethod("GET");
					Connect.setRequestProperty("Connection", "Keep-Alive");
					Connect.setUseCaches(false);
					Connect.setDoInput(true);
					Connect.setRequestProperty("Cookie", cookies);
					Connect.connect();

					Log.i("cookie", cookies);

					source = new Source(Connect);
					List<Element> els = source.getAllElements();
					Pattern sourceP = Pattern.compile("href=\"(.*?)\" ");// ADD
																			// source
																			// pattern
					Matcher matcher = sourceP.matcher(els.get(94).toString());
					String TimeTableSource = "";
					while (matcher.find()) {
						TimeTableSource = els
								.get(94)
								.toString()
								.substring(matcher.start() + 13,
										matcher.end() - 2);
						Log.i("text",
								els.get(94)
										.toString()
										.substring(matcher.start(),
												matcher.end()));
						temp = TimeTableSource;
						Log.i("temp", temp);
					}
					Message message = mHandler.obtainMessage(5, temp);
					mHandler.sendMessage(message);

				} catch (Exception e) {
					Log.e("TTerr2", "TTerr", e);
					flag = true;
				}
			}
		}.start();
	}

	public void GetTimeTable2(final String TTadd) {
		new Thread() {
			public void run() {
				Source source = null;
				try {
					String TTURL = url.toString() + TTadd;
					URL Timetable = new URL(TTURL);
					Connect.setInstanceFollowRedirects(false);
					Connect = (HttpURLConnection) Timetable.openConnection();
					Connect.setRequestMethod("GET");
					Connect.setRequestProperty("Connection", "Keep-Alive");
					Connect.setUseCaches(false);
					Connect.setDoInput(true);
					Connect.setRequestProperty("Cookie", cookies);
					Connect.connect();

					Log.i("cookie", cookies);

					source = new Source(Connect);
					source.fullSequentialParse();
					Element tr, td;
					int i, j;
					Element table = source
							.getAllElements(HTMLElementName.TABLE).get(5);
					for (i = 2; i < 15; i++) {
						tr = table.getAllElements(HTMLElementName.TR).get(i); // i번째
																				// <tr>태그내용을
																				// tr
																				// Element로
																				// 저장

						for (j = 0; j < 7; j++) {
							td = tr.getAllElements(HTMLElementName.TD).get(j); // 해당<tr>안의
																				// j번째
																				// <td>의
																				// 내용을
																				// td
																				// Element로
																				// 저장
							all[i - 2][j] = td.getTextExtractor().toString(); // td값을
																				// String으로
																				// all배열안에
																				// 저장함
						}
						j = 0;
					}
					mHandler.sendEmptyMessage(6);
					Log.i("cookie", "what");

				} catch (Exception e) {
					Log.e("TTerr1", "TTerr", e);
					mHandler.sendEmptyMessage(7);
					flag = true;
				}
			}
		}.start();
	}

	public void TableData(String[][] p_data) throws IOException {
		File savefile;
		int i, j;
		savefile = new File("text.txt");
		Log.i("errr", savefile.getPath());

		try {
			FileOutputStream fos = openFileOutput(savefile.getPath(),
					MODE_PRIVATE);
			for (i = 0; i < 13; i++)
				for (j = 0; j < 7; j++)
					fos.write((p_data[i][j]).getBytes());

			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private Runnable mRunnable = new Runnable() {
		public void run() {
			TableDataManager manager = new TableDataManager(
					getApplicationContext());
			manager.insertData(all);
		}
	};

	public void onBackPressed() {
		super.onBackPressed();

		Thread.currentThread();
		Thread.interrupted();

	}
}
