package com.gnu_app;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.TextView;

public class WidgetService extends Activity{
	TextView subject1 = (TextView)findViewById(R.id.num1_2);
	TextView subject2 = (TextView)findViewById(R.id.num2_2);
	TextView subject3 = (TextView)findViewById(R.id.num3_2);
	TextView subject4 = (TextView)findViewById(R.id.num4_2);
	TextView subject5 = (TextView)findViewById(R.id.num5_2);
	TextView subject6 = (TextView)findViewById(R.id.num6_2);
	TextView subject7 = (TextView)findViewById(R.id.num7_2);
	TextView subject8 = (TextView)findViewById(R.id.num8_2);
	TextView subject9 = (TextView)findViewById(R.id.num9_2);
	TextView subject10 = (TextView)findViewById(R.id.num10_2);
	
	
	private Context context ;
	
	public WidgetService(Context context){
		this.context = context;
	}

	public void Widget(int i, String string){
		if(string!=null){
			Log.i("sub","sett");
			if(i==0) subject1.setText(string);				
			if(i==1) subject2.setText(string);
			if(i==2) subject3.setText(string);
			if(i==3) subject4.setText(string);
			if(i==4) subject5.setText(string);
			if(i==5) subject6.setText(string);
			if(i==6) subject7.setText(string);
			if(i==7) subject8.setText(string);
			if(i==8) subject9.setText(string);
			if(i==9) subject10.setText(string);
		}
	}
	
}