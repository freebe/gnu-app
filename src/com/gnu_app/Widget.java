package com.gnu_app;

import java.util.Calendar;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.RemoteViews;

public class Widget extends AppWidgetProvider {
	private SQLiteDatabase db;
	public OpenHelper Opener;
	private static final String dbName = "TimeTable.db";
    private static final String tableName = "Schedule";
    public static final int dbVersion = 1;
    public int week;
    Calendar cal;
    Context context;
    RemoteViews remoteViews;
    public static final String Mon_CLICK = "MON_CLK";
    public static final String Tue_CLICK = "TUE_CLK";
    public static final String Wen_CLICK = "WEN_CLK";
    public static final String Thu_CLICK = "THU_CLK";
    public static final String Fri_CLICK = "FRI_CLK";
    public static final String Sat_CLICK = "SAT_CLK";
    private int day[] = {R.id.mon,R.id.Tue,R.id.Wen,R.id.Thu,R.id.Fri,R.id.Sat};
    
    private class OpenHelper extends SQLiteOpenHelper {
        public OpenHelper(Context context, String name, CursorFactory factory,
                int version) {
            super(context, name, null, version);
        }
        public void onCreate(SQLiteDatabase arg0) {
        }
        public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
        }
    }
    
	public void onEnabled(Context context){
		super.onEnabled(context);
	}
	
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds){
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		
		openDB(context);
        
		remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget);
		
        intentSet(context);
        
		for(int i=0; i<appWidgetIds.length; i++){ 
            int appWidgetId = appWidgetIds[i];
            appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
        } // 위젯마다 고유의 id 때문에 변화가 생길경우 모든 위젯을 update 해준다고 함 사실 모르겠음 ..
		cal = Calendar.getInstance();
		week = cal.get(Calendar.DAY_OF_WEEK);
		today(week);
		if(week!=1) load(week);//일요일이 1 월요일 2...
		}

	public void openDB(Context context){
		this.context=context;
		this.Opener = new OpenHelper(context, dbName, null, dbVersion);
        db = Opener.getReadableDatabase();
		
	}
	
	public void intentSet(Context context){
		Intent Mon_intent = new Intent(context,Widget.class);
        Mon_intent.setAction(Widget.Mon_CLICK);
		PendingIntent Mon_pendingIntent = PendingIntent.getBroadcast(context, 0, Mon_intent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.mon, Mon_pendingIntent);
        
        Intent Tue_intent = new Intent(context,Widget.class);
        Tue_intent.setAction(Widget.Tue_CLICK);
		PendingIntent Tue_pendingIntent = PendingIntent.getBroadcast(context, 0, Tue_intent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.Tue, Tue_pendingIntent);
        
        Intent Wen_intent = new Intent(context,Widget.class);
        Wen_intent.setAction(Widget.Wen_CLICK);
		PendingIntent Wen_pendingIntent = PendingIntent.getBroadcast(context, 0, Wen_intent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.Wen, Wen_pendingIntent);
        
        Intent Thu_intent = new Intent(context,Widget.class);
        Thu_intent.setAction(Widget.Thu_CLICK);
		PendingIntent Thu_pendingIntent = PendingIntent.getBroadcast(context, 0, Thu_intent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.Thu, Thu_pendingIntent);
        
        Intent Fri_intent = new Intent(context,Widget.class);
        Fri_intent.setAction(Widget.Fri_CLICK);
		PendingIntent Fri_pendingIntent = PendingIntent.getBroadcast(context, 0, Fri_intent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.Fri, Fri_pendingIntent);
        
        Intent Sat_intent = new Intent(context,Widget.class);
        Sat_intent.setAction(Widget.Sat_CLICK);
		PendingIntent Sat_pendingIntent = PendingIntent.getBroadcast(context, 0, Sat_intent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.Sat, Sat_pendingIntent);
	}
	
	public void onDeleted(Context context, int[] appWidgetIds){
		super.onDeleted(context, appWidgetIds);
	}
	
	public void load(int week){
		
		int num, i;
		Cursor cursor;
		for(i=0; i<11;i++){
			num = i * 7 + week;
		
		String selectSql = "SELECT Subject " + 
							"From " + tableName +
							" where " + "id = '" + num + "' ;";
		 cursor = db.rawQuery(selectSql, null);
		 while (cursor.moveToNext()) {
				 String A = cursor.getString(0);
					 remote(A,i);
				 }
		 }
		
	}
	
	private void today(int week) {
		int i=0;
		ComponentName thisWidget = new ComponentName(context, Widget.class);
		AppWidgetManager manager = AppWidgetManager.getInstance(context);
		for(i=0; i<6; i++){
			 remoteViews.setTextColor(day[i], 0xFF336600);
		 }// 날짜 색상 다 초기화
		if(week==1){
			 remoteViews.setTextViewText(R.id.Sat, "토");
			 remoteViews.setTextColor(R.id.Sat, 0xFF336600);			 
			 } 
		 if(week==2){
			 remoteViews.setTextViewText(R.id.mon, "<월>");
			 remoteViews.setTextColor(R.id.mon, 0xFFFF3300);
			 }
		 if(week==3){
			 remoteViews.setTextViewText(R.id.Tue, "<화>");
			 remoteViews.setTextColor(R.id.Tue, 0xFFFF3300);
			 remoteViews.setTextViewText(R.id.mon, "월");
			 }
		 if(week==4){
			 remoteViews.setTextViewText(R.id.Wen, "<수>"); //바뀌는 날
			 remoteViews.setTextColor(R.id.Wen, 0xFFFF3300);// 빨강
			 remoteViews.setTextViewText(R.id.Tue, "화");
			 }
		 if(week==5){
			 remoteViews.setTextViewText(R.id.Thu, "<목>");
			 remoteViews.setTextColor(R.id.Thu, 0xFFFF3300);
			 remoteViews.setTextViewText(R.id.Wen, "수");
			 }
		 if(week==6){
			 remoteViews.setTextViewText(R.id.Fri, "<금>");
			 remoteViews.setTextColor(R.id.Fri, 0xFFFF3300);
			 remoteViews.setTextViewText(R.id.Thu, "목");
			 }
		 if(week==7){
			 remoteViews.setTextViewText(R.id.Sat, "<토>");
			 remoteViews.setTextColor(R.id.Sat, 0xFFFF3300);
			 remoteViews.setTextViewText(R.id.Fri, "금");
			 }
		 manager.updateAppWidget(thisWidget, remoteViews);
	}

	private void remote(String a, int i) {
		ComponentName thisWidget = new ComponentName(context, Widget.class);
		AppWidgetManager manager = AppWidgetManager.getInstance(context);
		String Data="";
		String Data2="";
		int subjectIndex = a.indexOf('/');
		int classIndex = a.indexOf('/');
		System.out.println(a);
		if(subjectIndex!=-1){
			String temp=a.substring(0,subjectIndex);
			Data=temp;
			temp=a.substring(classIndex+2);
			Data2=temp;
		}
		
		 if(i==1){
			 remoteViews.setTextViewText(R.id.num1_2, Data);
			 remoteViews.setTextViewText(R.id.num1_3, Data2);
		 }
		 if(i==2){
			 remoteViews.setTextViewText(R.id.num2_2, Data);
			 remoteViews.setTextViewText(R.id.num2_3, Data2);
		 }
		 if(i==3){
			 remoteViews.setTextViewText(R.id.num3_2, Data);
			 remoteViews.setTextViewText(R.id.num3_3, Data2);
		 }
		 if(i==4){
			 remoteViews.setTextViewText(R.id.num4_2, Data);
			 remoteViews.setTextViewText(R.id.num4_3, Data2);
		 }
		 if(i==5){
			 remoteViews.setTextViewText(R.id.num5_2, Data);
			 remoteViews.setTextViewText(R.id.num5_3, Data2);
		 }
		 if(i==6){
			 remoteViews.setTextViewText(R.id.num6_2, Data);
			 remoteViews.setTextViewText(R.id.num6_3, Data2);
		 }
		 if(i==7){
			 remoteViews.setTextViewText(R.id.num7_2, Data);
			 remoteViews.setTextViewText(R.id.num7_3, Data2);
		 }
		 if(i==8){
			 remoteViews.setTextViewText(R.id.num8_2, Data);
			 remoteViews.setTextViewText(R.id.num8_3, Data2);
		 }
		 if(i==9){
			 remoteViews.setTextViewText(R.id.num9_2, Data);
			 remoteViews.setTextViewText(R.id.num9_3, Data2);
		 }
		 if(i==10){
			 remoteViews.setTextViewText(R.id.num10_2, Data);
			 remoteViews.setTextViewText(R.id.num10_3, Data2);
		 }
		 manager.updateAppWidget(thisWidget, remoteViews);
	}

	public void onReceive(Context context, Intent intent)
	  {
		super.onReceive(context, intent);
	    String action = intent.getAction();
	    remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget);
	    
	    int i=0;
	    for(i=0; i<6; i++){
			 remoteViews.setTextColor(day[i], 0xFF336600);
		 }
	    
	    if(action.equals(Mon_CLICK)==true)
	    {
	    	Intent intentUpdate = new Intent(context,Widget.class);
	    	remoteViews.setTextColor(R.id.mon, 0xFFFF3300);
	    	openDB(context);
	    	load(2);
            context.startService(intentUpdate);
	      Log.w("mon", "mon");
	    }
	    if(action.equals(Tue_CLICK)==true)
	    {
	    	Intent intentUpdate = new Intent(context,Widget.class);
	    	remoteViews.setTextColor(R.id.Tue, 0xFFFF3300);
	    	openDB(context);
	    	load(3);
            context.startService(intentUpdate);
            
	      Log.w("mon", "Tue_CLICK");
	    }
	    if(action.equals(Wen_CLICK)==true)
	    {
	    	Intent intentUpdate = new Intent(context,Widget.class);
	    	remoteViews.setTextColor(R.id.Wen, 0xFFFF3300);
	    	openDB(context);
	    	load(4);
            context.startService(intentUpdate);
            
	      Log.w("mon", "Wen_CLICK");
	    }
	    if(action.equals(Thu_CLICK)==true)
	    {
	    	Intent intentUpdate = new Intent(context,Widget.class);
	    	remoteViews.setTextColor(R.id.Thu, 0xFFFF3300);
	    	openDB(context);
	    	load(5);
            context.startService(intentUpdate);
	      Log.w("mon", "Thu_CLICK");
	    }
	    if(action.equals(Fri_CLICK)==true)
	    {
	    	Intent intentUpdate = new Intent(context,Widget.class);
	    	remoteViews.setTextColor(R.id.Fri, 0xFFFF3300);
	    	openDB(context);
	    	load(6);
            context.startService(intentUpdate);
	      Log.w("mon", "Fri_CLICK");
	    }
	    if(action.equals(Sat_CLICK)==true)
	    {
	    	Intent intentUpdate = new Intent(context,Widget.class);
	    	remoteViews.setTextColor(R.id.Sat, 0xFFFF3300);
	    	openDB(context);
	    	load(7);
            context.startService(intentUpdate);
	      Log.w("mon", "Sat_CLICK");
	    }
	    
	    
	    // 위젯 업데이트 인텐트를 수신했을 때
	    if(action.equals("android.appwidget.action.APPWIDGET_UPDATE"))
	    {
	      Log.w("생성", "생성");
	    }
	    // 위젯 제거 인텐트를 수신했을 때
	    else if(action.equals("android.appwidget.action.APPWIDGET_DISABLED"))
	    {
	      Log.w("제거", "제거");
	    }
	  }
	
}
